#include "app.hpp"
#include <algorithm>

#define HISTOGRAM_SIZE 256
#define BLOCK_SIZE 1024


namespace IMAC
{
	// TODO calculateHistogram

	// __global__ void calculateHistogram()
	// {
	// 	uint tidx = (blockIdx.x * blockDim.x) + threadIdx.x;
	// 	uint tidy = (blockIdx.y * blockDim.y) + threadIdx.y;
		
	// }

	__device__
	float4 convertPixel2HSV(uchar4 pixel) {
		float r, g, b, a;
		float h, s, v;
		
		r = pixel.x / 255.0f;
		g = pixel.y / 255.0f;
		b = pixel.z / 255.0f;
		a = pixel.w;
		
		float max = fmax(r, fmax(g, b));
		float min = fmin(r, fmin(g, b));
		float diff = max - min;
		
		v = max;
		
		if(v <= 0.0f) { // black
			h = s = 0.0f;
		} else {
			s = diff / v;
			if(diff < 0.001f) { // grey
				h = 0.0f;
			} else { // color
				if(max == r) {
					h = 60.0f * (g - b)/diff;
					if(h < 0.0f) { h += 360.0f; }
				} else if(max == g) {
					h = 60.0f * (2 + (b - r)/diff);
				} else {
					h = 60.0f * (4 + (r - g)/diff);
				}
			}		
		}
		
		return (float4) {h, s, v, a};
		//return (float4) {r, g, b, a};
	}

	__device__ uchar4 convertPixel2RGB(float4 pixel) {
		float r, g, b;
		float h, s, v;
		
		h = pixel.x;
		s = pixel.y;
		v = pixel.z;
		
		float f = h/60.0f;
		float hi = floorf(f);
		f = f - hi;
		float p = v * (1 - s);
		float q = v * (1 - s * f);
		float t = v * (1 - s * (1 - f));
		
		if(hi == 0.0f || hi == 6.0f) {
			r = v;
			g = t;
			b = p;
		} else if(hi == 1.0f) {
			r = q;
			g = v;
			b = p;
		} else if(hi == 2.0f) {
			r = p;
			g = v;
			b = t;
		} else if(hi == 3.0f) {
			r = p;
			g = q;
			b = v;
		} else if(hi == 4.0f) {
			r = t;
			g = p;
			b = v;
		} else {
			r = v;
			g = p;
			b = q;
		}
		
		unsigned char red = (unsigned char) (255.0f * r);
		unsigned char green = (unsigned char) (255.0f * g);
		unsigned char blue = (unsigned char) (255.0f * b);
		return (uchar4) {red, green, blue, 255};
	}


	__global__ 
	void convert2HSV(uchar4 *rgb, float4 *hsv, int imgWidth, int imgHeight){
		uint tidx = (blockIdx.x * blockDim.x) + threadIdx.x;
		uint tidy = (blockIdx.y * blockDim.y) + threadIdx.y;

		if(tidx < imgWidth && tidy < imgHeight) {
			uchar4 rgb_pixel = rgb[tidx + imgWidth*tidy];
			float4 hsv_pixel = convertPixel2HSV(rgb_pixel);
			hsv[tidx + imgWidth*tidy] = hsv_pixel;
		}
	}

	__global__ 
	void convert2RGB(uchar4 *rgb, float4 *hsv, int imgWidth, int imgHeight){
		int tidx = threadIdx.x + blockIdx.x * blockDim.x;
		int tidy = threadIdx.y + blockIdx.y * blockDim.y;
		
		if(tidx < imgWidth && tidy < imgHeight) {
			float4 hsv_pixel = hsv[tidx + imgWidth*tidy];
			uchar4 rgb_pixel = convertPixel2RGB(hsv_pixel);
			rgb[tidx + imgWidth * tidy] = rgb_pixel;
		}

		// if(tidx < imgWidth && tidy < imgHeight) {
		// 	float4 hsv_pixel = hsv[tidx + imgWidth*tidy];
		// 	rgb[tidx + imgWidth * tidy] = (uchar4){255.0*hsv_pixel.x, 255.0*hsv_pixel.y, 255.0*hsv_pixel.z, 255};
		// }
	}


	__global__
	void calculateHistogram(float4 *imgDataBuffer, float *histogram, int size){
        __shared__ unsigned int cache[HISTOGRAM_SIZE];
        if (threadIdx.x < HISTOGRAM_SIZE) {
            cache[threadIdx.x] = 0;
        }
        __syncthreads();

        // Used interleaved partitioning.
        // All threads process a contiguous section of elements.
        // They all move to the next section and repeat.
        // The memory accesses are coalesced.
        int tid = threadIdx.x + blockDim.x * blockIdx.x;
        // Define stride. Stride is the total number of threads.
        int stride = blockDim.x * gridDim.x;

        while (tid < size) {
            atomicAdd(&(cache[(int)imgDataBuffer[tid].z]), 1);
            tid += stride;
        }
        __syncthreads();

        if (threadIdx.x < HISTOGRAM_SIZE) {
            atomicAdd(&(histogram[threadIdx.x]), cache[threadIdx.x]);
        }
	}

	void histogramEqualization(const std::vector<uchar4> &inputImg, // Input image
		const uint imgWidth, const uint imgHeight, // Image size
		std::vector<uchar4> &output // Output image
		)
	{
		//float4 *h_hsvImg = NULL;

		float4 *d_hsvImg = NULL;
		uchar4 *d_rgbImg = NULL;
		uchar4 *d_rgbTestImg = NULL;
		float *d_histogram = NULL;

		size_t bytesUchar4 = (imgWidth*imgHeight)*sizeof(uchar4);
		size_t bytesFloat4 = (imgWidth*imgHeight)*sizeof(float4);

		dim3 threadPerBlocks(16,16);
		dim3 numBlocks(ceil((float)imgWidth/threadPerBlocks.x), ceil((float)imgHeight/threadPerBlocks.y));

		cudaMalloc((void **) &d_rgbImg, bytesUchar4);
		cudaMalloc((void **) &d_hsvImg, bytesFloat4);

		cudaMemcpy(d_rgbImg, inputImg.data(), bytesUchar4, cudaMemcpyHostToDevice);

		convert2HSV<<<numBlocks, threadPerBlocks>>>(d_rgbImg, d_hsvImg, imgWidth, imgHeight);
		//convert2RGB<<<numBlocks, threadPerBlocks>>>(d_rgbTestImg, d_hsvImg, imgWidth, imgHeight); // Check if the image generated is all right.
		cudaFree(d_rgbImg);

		size_t histogramBytes = HISTOGRAM_SIZE * sizeof(float);
		cudaMalloc((void **)&d_histogram, histogramBytes);

		calculateHistogram<<<numBlocks, threadPerBlocks>>>(d_hsvImg, d_histogram, imgWidth*imgHeight);


		cudaFree(d_hsvImg);
		cudaFree(d_histogram);
	}

}
